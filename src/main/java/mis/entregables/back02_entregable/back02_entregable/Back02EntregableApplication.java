package mis.entregables.back02_entregable.back02_entregable;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Back02EntregableApplication {

	public static void main(String[] args) {
		SpringApplication.run(Back02EntregableApplication.class, args);
	}

}
