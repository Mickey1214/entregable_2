package mis.entregables.back02_entregable.back02_entregable.servicio;

import mis.entregables.back02_entregable.back02_entregable.modelo.Producto;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface RepositorioProducto extends MongoRepository<Producto, String> {
}
