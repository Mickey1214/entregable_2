package mis.entregables.back02_entregable.back02_entregable.servicio;

import mis.entregables.back02_entregable.back02_entregable.modelo.Producto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class ProductoServicio {

    //Atributo
    @Autowired
    RepositorioProducto productoRepository;

    public List<Producto> findAll() {
        return productoRepository.findAll();
    }

    public Optional<Producto> findById(String  id) {
        return productoRepository.findById(id);
    }

    public Producto save(Producto entity) {
        return productoRepository.save(entity);
    }

    public boolean delete(Producto entity) {
        try {
            productoRepository.delete(entity);
            return true;
        } catch(Exception ex) {
            return false;
        }
    }
}
