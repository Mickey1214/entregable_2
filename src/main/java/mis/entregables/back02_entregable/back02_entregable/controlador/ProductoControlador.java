package mis.entregables.back02_entregable.back02_entregable.controlador;

import mis.entregables.back02_entregable.back02_entregable.modelo.Producto;
import mis.entregables.back02_entregable.back02_entregable.servicio.ProductoServicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
//Agregar ruta base de las peticiones
@RequestMapping("${url.base}")
public class ProductoControlador {

    //Atributo
    @Autowired
    ProductoServicio productoService;

    //Metodo/Peticion que regresa la lista entera de los productos de la bd
    @GetMapping("/productos")
    public List<Producto> getProductos() {
        return productoService.findAll();
    }

    //Metodo/Peticion que regresa un producto en especifico de acuerdo su id
    @GetMapping("/productos/{id}" )
    public Optional<Producto> getProductoId(@PathVariable String id){
        return productoService.findById(id);
    }

    //Metodo/Peticion que agrega un producto dentro de la bd
    @PostMapping("/productos")
    public Producto postProductos(@RequestBody Producto newProducto){
        productoService.save(newProducto);
        return newProducto;
    }

    //Metodo/Peticion que modifica el producto
    @PutMapping("/productos")
    public void putProductos(@RequestBody Producto productoToUpdate){
        productoService.save(productoToUpdate);
    }

    //Metodo/Peticion que se encarga de eliminar un producto
    @DeleteMapping("/productos")
    public boolean deleteProductos(@RequestBody Producto productoToDelete){
        return productoService.delete(productoToDelete);
    }
}
